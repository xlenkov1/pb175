const express = require('express');
const app = express();
const router = require('./router/routes');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(bodyParser.urlencoded({limit: '10mb', extended: false}));
app.use(express.static('public'));


let cookieParser = require('cookie-parser');
app.use(cookieParser());

// Database setup
mongoose.connect(process.env.DATABASE_URL);

const db = mongoose.connection;
db.on('error', error => console.log(error));
db.once('open', () => console.log('Connected to Mongoose'));

app.use('/', router);

// Server listening
const port = process.env.PORT;
app.listen(port, () => {console.log(`Listening on ${port}`)});