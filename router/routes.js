const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const auth = require('../middleware/auth.js');
const { check, validationResult } = require('express-validator');
require('dotenv').config();

const authSecret = process.env.VSD;


router.post('/register', [
    check('heslo', 'Password must me 6+ characters long')
        .exists()
        .isLength({ min: 6 }),
    check('email', 'Email is not valid')
        .isEmail()
        .normalizeEmail()
    ], async (req, res) => {

    let alert = [];
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        alert = errors.array();
        return res.status(400).render('register', { alert });
    }

    const { email, heslo, hesloZnova } = req.body;
    const password = heslo;

    if (heslo !== hesloZnova) {
        alert.push({ msg: "Passwords don't match" });
        return res.status(400).render('register', { alert });
    }

    let dupCheck = await User.findOne({ email: email });
    if (dupCheck) {
        alert.push({ msg: "User already exists" });
        return res.status(400).render('register', { alert });
    }

    const user = new User({
        email, 
        password
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    try {
        await user.save();
    } catch (error) {
        alert.push({ msg: "Database error, contact admin" });
        return res.status(400).render('register', { alert });
    }

    const payload = {
        user: {
            email: email
        }
    };

    const authToken = jwt.sign(payload, authSecret);
    return res.cookie('authToken', authToken, { maxAge: 86400000 })
              .status(200).redirect('/home');
});


router.get('/register', (req, res) => {
    const { authToken } = req.cookies;

    if (authToken) {
        return res.redirect('/home');
    }

    res.render('register');
});


router.get('/home', auth, async (req, res) => {
    const { email } = req.user;
    let user = await User.findOne({ email: email });
    res.render('home', { user });
});


router.post('/addNote', auth, async (req, res) => {
    const { addNote } = req.body;
    const { email } = req.user;
    let user = await User.findOne({ email: email });
    const id = user.activeNotes.length + user.deletedNotes.length;
    user.activeNotes.push({ id: id, note: addNote });
    await User.findOneAndUpdate({ email: email }, user);
    res.redirect('/home');
})


router.get('/done', auth, async (req, res) => {
    const { email } = req.user;
    let user = await User.findOne({ email: email });
    res.render('done', { user });
});


router.post('/done', auth, async (req, res) => {
    const { email } = req.user;
    let user = await User.findOne({ email: email });

    for (const [key, value] of Object.entries(req.body)) {
        let done = user.activeNotes.filter((el) => (el.id === parseInt(key)));
        user.deletedNotes.push(done[0]);
        user.activeNotes = user.activeNotes
            .filter((el) => (el.id !== parseInt(key)));
    }
    await User.findOneAndUpdate({ email: email }, user);
    res.redirect('/home');
});


router.get('/admin', auth, async (req, res) => {
    const { email } = req.user;
    let user = await User.findOne({ email: email });
    if (user.role !== "Admin") {
        res.redirect('/home');
    }
    const allUsers = await User.find();
    res.render('admin', { user, allUsers });
});


router.post('/deleteUsers', auth, async (req, res) => {
    const { email } = req.user;
    let user = await User.findOne({ email: email });
    if (user.role !== "Admin") {
        res.redirect('/home');
    }
    for (const [key, value] of Object.entries(req.body)) {
        await User.deleteOne({ email: key });
    }

    res.redirect('/admin');
});


router.get('/logout', auth, (req, res) => {
    res.clearCookie("authToken");
    res.redirect('/');
});


router.post('/', [
    check('heslo', 'Password must me 6+ characters long')
        .exists()
        .isLength({ min: 6 }),
    check('email', 'Email is not valid')
        .isEmail()
    ], async (req, res) => {

    let alert = [];
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        alert = errors.array();
        return res.status(400).render('index', { alert });
    }

    const { email, heslo } = req.body;
    const password = heslo;

    try {
        let user = await User.findOne({ email });

        if (!user) {
            alert.push({ msg: "User was not found" });
            return res.status(400).render('index', { alert });
        }

        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            alert.push({ msg: "Wrong password" });
            return res.status(400).render('index', { alert });
        }
    } catch (error) {
        console.error(error.message);
        alert.push({ msg: "Server error" });
        return res.status(400).render('index', { alert });
    }

    const payload = {
        user: {
            email: email
        }
    };

    const authToken = jwt.sign(payload, authSecret);
    return res.cookie('authToken', authToken, { maxAge: 86400000 })
              .status(200).redirect('/home');
});


router.get('/', (req, res) => {
    const { authToken } = req.cookies;

    if (authToken) {
        return res.redirect('/home');
    }

    res.render('index');
});


module.exports = router;

