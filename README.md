`HW A SW NÁROKY:`
Na nainštalovanie webovej aplikácie je potreba mať stiahnutý a nainštalovaný MongoDB
(+ prípadné GUI na prácu s MongoDB, v tejto inštalácií bude použité Visual Studio Code IDE), 
Node.js a NPM.

`POPIS INŠTALÁCIE:`
Inštalačný balík obsahuje zdrojové súbory k inštalácií aplikácie. 

Ďalšie operácie sa budú odohrávať vo VSC. 
Po spustení VSC si otvoríme terminál v koreňovej zložke projektu. 
Inštaláciu zahájime príkazom `npm install`. Tým sa stiahnu potrebné node-modules.

V rozšíreniach si stiahneme MongoDB for VSC. 
Po otvorení rozšírenia na ľavej lište, dáme Connect a zadáme napríklad `mongodb://localhost:27017/projekt`
Týmto zapneme databázový systém. 

V súbore env.local.example si nastavíme premenné prostredia. 
PORT je port, na ktorom nám server lokálne pobeží,
DATABASE_URL je url rovnaká akú sme použili pri spustení databázy MongoDB vyššie a 
VSD je náš secret na šifrovanie hesiel. 
Súbor premenujeme na `.env`
Príkazom `node server.js` v terminály spustíme aplikáciu.	

Admina aplikácie vytvoríme tak, že sa zaregistrujeme normálne ako Používateľ a rolu nastavíme cez databázu, kde zmeníme rolu na „Admin“.

