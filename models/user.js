const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    
    email: {
      type: String,
      required: true,
      unique: true
    },
    
    role: {
      type: String,
      required: true,
      default: "User"
    },
    
    password: {
      type: String,
      required: true
    },

    activeNotes: {
        type: [
            "Mixed"
          ],
        default: []
    },
    
    deletedNotes: {
        type: [
            "Mixed"
          ],
        default: []
    },

  });

module.exports = mongoose.model('user', UserSchema);
