const jwt = require('jsonwebtoken');
require('dotenv').config();

const authSecret = process.env.VSD;

module.exports = async(req, res, next) => {
    const { authToken } = req.cookies;
    if(!authToken) {
        return res.send({code: 403, message: "Forbiden"});
    }
    
    try {
        const decoded = jwt.verify(authToken, authSecret);
        req.user = decoded.user;
        next();

    } catch(error) {
        console.error(error);
        return res.send({code: 403, message: "Forbiden"});
    }
}
